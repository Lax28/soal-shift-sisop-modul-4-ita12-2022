# soal-shift-sisop-modul-4-ITA12-2022
## Anggota Kelompok
1. Muhammad Jovan Adiwijaya Yanuarsyah (5027201025)
2. Made Gede Krisna Wangsa (5027201047)
3. Nida'ul Faizah (5027201064)

## Description
Laporan ini dibuat dengan tujuan untuk menjelaskan pengerjaan serta permasalahan yang kami alami dalam pengerjaan soal shift sistem operasi modul 4 tahun 2022.

## Soal Shift 1
Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan berikut:
### A)
Semua direktori dengan awalan `Animeku_` akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13	
*Contoh* : 
>“Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt”

Langkah pertama adalah melakukan inialisasi nama directory

```c
char typeName[10] = "Animeku_";
```

Selanjutnya membuat fungsi encode dengan pengelompokan saat huruf besar atau huruf kecil. Ketika huruf besar maka akan di-encode menggunakan atbash cipher dan ketika huruf kecil akan di-encode dengan rot13.
Lakukan inialisasi pada fungsi encode

```c
int index, i = 0, countAlphabet = 26;
```

Dilakukan checking jika kosong

```c
if((strcmp(filename, ".") == 0) || (strcmp(filename, "..") == 0)) 
    {
        return;
    }
```

Hitung jumlah character pada string tersebut

```c
index = strlen(filename);
```


Lalu bedah setiap charnya untuk di encode menggunakan atbash ataupun rot13

```c
while (filename[i] != index + 1)
    {
        // Jika ada whitespace, maka continue dan index akan berlanjut
        if(filename[i] == ' ')
        {
            i++;
            continue;
        }

        // Jika Huruf Besar dengan ASCII (65 <= x <= 90)
        if(filename[i] >= 'A' && filename[i] <= 'Z')
        {
            // Di encode menggunakan ALbash Cipher
            for(int j = 0; j < index; j++)
            {
                int start = filename[j];
                filename[j] = countAlphabet - filename[j] - 64;
                filename[j] += 65;
            }
        } else if (filename[i] >= 'a' && filename[i] <= 'z') // Jika huruf kecil dengan ASCII (97 <= x <= 122)
        {
            // Di encode menggunakan rot13
            // Carriage Return = 13 -> ASCII ('CR' = 13)
            if(filename[i] + 13 > 'z')
            {
                filename[i] -= 13;
            } else 
            {
                filename[i] += 13;
            }
        }
        // lanjut ke index selanjutnya
        i++;
    }
```

### B)
Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.
```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    // Inisialisasi
    int res = 0;
    struct dirent *dir;

    // Mencari string yang diinginkan
    char *letter = strstr(path, typeName), fpath[1000];
    
    // Melakukan pengecekan jika null
    if(letter != NULL)
    {
        decode(letter);
    }

    // Jika string terdapat '/' maka nilai path akan menjadi dirpath
    if(strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    // Inisialisasi
    DIR *dp;
    (void) fi;
    (void) offset;

    // Membuka directory
    dp = opendir(fpath);

    // mengembalikan kode kesalahan terakhir
    if(dp == NULL) 
    {
        return -errno;
    }

    // Membaca directory
    dir = readdir(dp);

    // Jika tidak null
    while (dir != NULL)
    {
        // Inisialisasi
        struct stat st;

        // menetapkan nilai satu byte ke blok memori byte demi byte.
        memset(&st, 0, sizeof(st));
        st.st_ino = dir->d_ino;
        st.st_mode = dir->d_type << 12;

        // Cek jika tidak null
        if(encode != NULL)
        {
            encode(dir->d_name);
        }

        res = filler(buf, dir->d_name, &st, 0);
        if(res != 0 )
        {
            break;
        }
    }

    // Tutup directory
    closedir(dp);
    return 0;
}
```

Ketika melakukan rename untuk file maka harus membaca file yang dibuka menggunakan fungsi `xmp_read` sebagai berikut
```c
// Membaca data dari file yang dibuka
static int xmp_read (const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    // Inisialisasi
    char fpath[1000];
    int fd, res;

    sprintf(fpath, "%s%s", dirpath, path);

    (void)fi;

    // Membuka path
    fd = open(fpath, O_RDONLY);

    // Mengirimkan error sesuai kode error terakhir
    if(fd == -1)
    {
        return -errno;
    }

    // Untuk membaca file
    res = pread(fd, buf, size, offset);

    // Mengirimkan error sesuai kode error terakhir
    if(res == -1)
    {
        res = -errno;
    }

    // menutup file
    close(fd);
    return res;
}
```

Untuk dapat melakukan rename maka dilakukan dengan fungsi xmp_getattr yaitu dengan tujuan mengubah sesuai dengan typeName yang diinisialisasi
```c
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char newPath[1000], *letter = strstr(path, typeName);

    // Di cek jika letter tidak sama dengan null atau kosong
    if(letter != NULL)
    {
        decode(letter);
    }

    sprintf(newPath, "%s%s", dirpath, path);

    // memberikan informasi rinci tentang file
    res = lstat(newPath, stbuf);

    // mengembalikan kode kesalahan terakhir
    if(res == -1)
    {
        return -errno;
    }
    
    return 0;
}
```

### C)
Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode

```c
int index, i = 0, countAlphabet = 26;
``

Selanjutnya dilakukan checking jika kosong atau NULL

```c
if((strcmp(filename, ".") == 0 ) || (strcmp(filename, "..") == 0) || (strstr(filename, "/") == NULL)) 
    {
        return;
    }
```

```c
char *name = strstr(filename, "/");
```

Dilakukan decode sesuai dengan ketentuan menggunakan atbash cipher dan rot13
```c
while (filename[i] != index + 1)
    {
        // Jika ada tanda /, maka akan berhenti
        if(filename[i] == '/')
        {
            break;
        }
        
        // Jika ada tanda ., maka akan berhenti dan index disimpan dengan i yang sekarang
        if(filename[i] == '.')
        {
            index = i;
            break;
        }

        // Jika Huruf Besar dengan ASCII (65 <= x <= 90)
        if(filename[i] >= 'A' && filename[i] <= 'Z')
        {
            // Meng-decode menggunakan ALbash Cipher
            for(int j = 0; j < index; j++)
            {
                int start = name[j] + 1;
                name[j] = countAlphabet - name[j] + 1 - 64;
                name[j] += 65;
            }
        } else if (filename[i] >= 'a' && filename[i] <= 'z') // Jika huruf kecil dengan ASCII (97 <= x <= 122)
        {
            // Meng-decode menggunakan rot13
            // Carriage Return = 13
            if(filename[i] - 13 > 'a')
            {
                filename[i] += 13;
            } else 
            {
                filename[i] -= 13;
            }
        }
        i++;
    }
```
Selanjutnya untuk rename sama dengan 1B yaitu menggunakan `xmp_getatr`, `xmp_readdir`, dan `xmp_read`

### D)
Setiap data yang terencode akan masuk dalam file `Wibu.log` 

**Contoh isi**: 
>**RENAME TERENKRIPSI**
>
>`/home/[USER]/Downloads/hai` --> `/home/[USER]/Downloads/Animeku_hebat` 
>
>**RENAME TERDECODE**
>
>`/home/[USER]/Downloads/Animeku_` --> `/home/[USER]/Downloads/Coba`

```c
FILE *fileLog = fopen("home/kurniacf/Wibu.log", "a");
```

Setelah itu, dilakukan print sesuai dengan perintah soal

```c
fprintf(fileLog, "%s --> %s\n", previousPath, newPath);
```

Selanjutnya file ditutup


```c
fclose(fileLog);
```

### E)
Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)

**Note** : 

>filesystem berfungsi normal layaknya linux pada umumnya, Mount source (root) filesystem adalah directory /home/[USER]/Documents,dalam penamaan file ‘/’ diabaikan, dan ekstensi tidak perlu di-encode
>
>Referensi : https://www.base64encode.org/ https://rot13.com/

```c
dir = readdir(dp);
```

Selanjutnya dilakukan perulangan dengan while jika directory tidak null

```c
while (dir != NULL)
    {
        // Inisialisasi
        struct stat st;

        // menetapkan nilai satu byte ke blok memori byte demi byte.
        memset(&st, 0, sizeof(st));
        st.st_ino = dir->d_ino;
        st.st_mode = dir->d_type << 12;

        // Cek jika tidak null
        if(encode != NULL)
        {
            encode(dir->d_name);
        }

        res = filler(buf, dir->d_name, &st, 0);
        if(res != 0 )
        {
            break;
        }
    }
```


## Soal Shift 2
Saat Anya sedang sibuk mengerjakan programnya, tiba-tiba Innu datang ke rumah Anya untuk mengembalikan barang yang dipinjamnya. Innu adalah programmer jenius sekaligus teman Anya. Ia melihat program yang dibuat oleh Anya dan membantu Anya untuk menambahkan fitur pada programnya dengan ketentuan sebagai berikut : 

### A)
Jika suatu direktori dibuat dengan awalan `IAN_[nama]`, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma Vigenere Cipher dengan key “INNUGANTENG” (Case-sensitive, Vigenere).

Cara kerja cipher Vigenere adalah dengan menjumlahkan notasi angka pada sebuah kata dengan notasi angka pada "key" dari cipher menurut alphabet. Jika huruf pada kata lebih banyak dari key, maka notasi angka penjumlahan huruf untuk huruf selanjutnya pada kata dilakukan dengan huruf pertama kembali pada key. Untuk decrypt cipher ini yaitu dengan melakukan hal yang berkebalikan dengan cara mengurangi huruf pada kata dengan huruf pada key. Implementasi cipher dan decipher Vigenere ini dapat dilakukan dengan cara sebagai berikut.

```c
void Cipher(char *word)
{
	
	char key[11]="INNUGANTENG";
	int input_len = strlen(word);
	for(int i=0; i<input_len; i++)
	{
		int j = i%11;
		if(word[i]>='A' && word[i]<='Z')
		{
			word[i]=(((word[i]-65)+(key[j]-65))%26)+65;
		}
		if(word[i]>='a' && word[i]<='z')
		{
			word[i]=(((word[i]-97)+(key[j]-65))%26)+97;
		}
	}
//Changes the word
}

void deCipher(char *word)
{
	
	char key[11]="INNUGANTENG";
	int input_len = strlen(word);
	for(int i=0; i<input_len; i++)
	{
		int j = i%11;
		if(word[i]>='A' && word[i]<='Z')
		{
			word[i]=(((word[i]-65)-(key[j]-65)+26)%26)+65;
		}
		else if(word[i]>='a' && word[i]<='z')
		{
			word[i]=(((word[i]-97)-(key[j]-65)+26)%26)+97;
		}
	}
//Changes the word
}
```

Dikarenakan case-sensitive, maka huruf besar dan kecil dibedakan. Huruf tersebut dikurangi dengan ascii yang 65 untuk kapital, 97 untuk huruf kecil. Kemudian hasil dijumlahkan dan dimod 26, karna alphabet terdapat sebanyak 26. Selanjutnya, ASCII dijumlahkan sesuai huruf akar menjadi alphabet kembali. Untuk decipher, akan ditambahkan 26 sebelum dimod untuk mengatasi masalah minus pada pengurangan.



### B)
Jika suatu direktori di rename dengan `IAN_[nama]`, maka seluruh isi dari direktori tersebut akan terencode seperti pada no. 2a.

```c
static int xmp_rename(const char *name_was, const char *name_is)
{
	update_log_rename(name_was, name_is);
	char *fpath= (char*) malloc(strlen(dirpath) + strlen(name_was) + 2);
	char *npath= (char*) malloc(strlen(dirpath) + strlen(name_is) + 2);
	sprintf(fpath, "%s%s",dirpath,name_was);
	sprintf(npath, "%s%s",dirpath,name_is);
	
	int foldStat = strncmp(name_is+1, "IAN_", 4);
	rename(fpath,npath);

	DIR *dp;
    struct dirent *de;

    dp = opendir(npath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
    	if(strcmp(de->d_name,".") && strcmp(de->d_name,".."))
    	{
    		if (de->d_type == DT_DIR)
            {
                char newpath[1024];

                strcpy(newpath, npath);
                strcat(newpath, "/");
                strcat(newpath, de->d_name);

                readSubdir(newpath, foldStat);

    			memset(newpath,0,strlen(newpath));
            }
    		char pathName1[1024];
    		char pathName2[1024];
    		char newName[50];
    		strcpy(newName, de->d_name);
    		(foldStat == 0) ? Cipher(newName) : deCipher(newName);
    		
    		strcpy(pathName2, npath);
    		strcat(pathName2,"/");
    		strcat(pathName2,newName);
    		
    		strcpy(pathName1, npath);
    		strcat(pathName1,"/");
    		strcat(pathName1,de->d_name);
    		rename(pathName1, pathName2);
    		
    		memset(pathName1,0,strlen(pathName1));
    		memset(pathName2,0,strlen(pathName2));
    		memset(newName,0,strlen(newName));
		}
    }
    closedir(dp);
    return 0;
}
```

Pada fungsi ini akan dibuat sebuat flag bernama foldStat yang menyimpan status dari folder yang akan direname. Jika setelah direname terdapat "IAN_", maka semua isi file akan dilakukan cipher(). Untuk melakukan proses tersebut pada semua sub-folder didalamnya, maka akan dilakukan rekursi agar semua file diproses. Hal ini dilakukan pada fungsi redSubdir(). Isi fungsi ini kurang lebih sama dengan fungsi renamenya, yaitu sebagai berikut.

```c
void readSubdir(const char *path, int resStat)
{
	DIR *dp;
    struct dirent *de;

    dp = opendir(path);

    if (dp == NULL) return;

    while ((de = readdir(dp)) != NULL) {
    	if(strcmp(de->d_name,".") && strcmp(de->d_name,".."))
    	{
    		if (de->d_type == DT_DIR)
            {
                char newpath[1024];

                strcpy(newpath, path);
                strcat(newpath, "/");
                strcat(newpath, de->d_name);

                readSubdir(newpath,resStat);

    			memset(newpath,0,strlen(newpath));
            }
    		char pathName1[1024];
    		char pathName2[1024];
    		char newName[50];
    		strcpy(newName, de->d_name);
    		(resStat == 0) ? Cipher(newName) : deCipher(newName);
    		
    		strcpy(pathName2, path);
    		strcat(pathName2,"/");
    		strcat(pathName2,newName);
    		
    		strcpy(pathName1, path);
    		strcat(pathName1,"/");
    		strcat(pathName1,de->d_name);
    		rename(pathName1, pathName2);
    		
    		memset(pathName1,0,strlen(pathName1));
    		memset(pathName2,0,strlen(pathName2));
    		memset(newName,0,strlen(newName));
		}
    }
    closedir(dp);
    return;
}
```


### C)
Apabila nama direktori dihilangkan `IAN_`, maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya. Jika folder yang berawalan "IAN_" direname sehingga awalan tersebut hilang, maka semua subfolder dan file didalamnya akan di `deCipher()`. Hal ini dapat dilakukan dengan menggunakan flag resStat yang sudah ada sebelumnya, dan merubah nama nama file dan folder menggunakan fungsi `deCipher()` yang sudah ada.

### D)
Untuk memudahkan dalam memonitor kegiatan yang dilakukan pada filesystem yang telah dibuat, ia membuat log system pada direktori `/home/[user]/hayolongapain_[kelompok].log`. Dimana log ini akan menyimpan daftar perintah system call yang telah dijalankan pada filesystem.

```c
int make_log_file()
{
	FILE* file_ptr = fopen(dirLog, "a");
    fclose(file_ptr);
    return 0;
}
```

### E)
Karena Innu adalah seorang perfeksionis, ia membagi isi dari log systemnya menjadi 2 level, yaitu level `INFO` dan `WARNING`. Untuk log level `WARNING`, digunakan untuk mencatat `syscall rmdir dan unlink`. Sisanya, akan dicatat pada level `INFO` dengan format sebagai berikut : 

>[Level]::[dd][mm][yyyy]-[HH]:[MM]:[SS]::[CMD]::[DESC :: DESC] 

**Keterangan** : 
>Level : Level logging
>
>dd : Tanggal
>
>mm : Bulan
>
>yyyy : Tahun
>
>HH : Jam (dengan format 24 Jam)	MM : Menit
>
>SS : Detik
>
>CMD : System call yang terpanggil
>
>DESC : Informasi dan parameter tambahan 

**Contoh** : 

>WARNING::21042022-12:02:42::RMDIR::/RichBrian 
>
>INFO::21042022-12:01:20::RENAME::/Song::/IAN_Song
>INFO::21042022-12:06:41::MKDIR::/BillieEilish

**NOTE** :
>Misalkan terdapat file bernama idontwannabeyouanymore.txt pada direktori BillieEilish. 
>
>“IAN_Song/BillieEilish/idontwannabeyouanymore.txt” → “IAN_Song/JvyfoeRbpvyp/qqbhzwngrnhmlbognlfsek.bkg”
>
>Referensi : https://www.dcode.fr/vigenere-cipher

Untuk memecahkan soal ini diperlukan sebuah fungsi `rmdir` dan `mkdir` pada FUSE seperti di bawah ini:

```c
static int xmp_mkdir (const char *name, mode_t dirMode)
{
	update_log("MKDIR", name);
	char *path= (char*) malloc(strlen(dirpath) + strlen(name) + 2);
	sprintf(path, "%s%s",dirpath,name);
	mkdir(path, dirMode);
	return 0;
}
static int xmp_rmdir (const char *name)
{
	char *nameFold = (char*) malloc(strlen(name));
	strcpy(nameFold, name);
	update_log("RMDIR", nameFold);
	char *path= (char*) malloc(strlen(dirpath) + strlen(name) + 2);
	sprintf(path, "%s%s",dirpath,name);
	rmtree(path);
	return 0;
}
```

Fungsi `rmdir` diperlukannya sebuah fungsi rekursif untuk menghapus semua file file didalamnya agar file dapat dihapur. Hal ini dikarenakan `remove()` hanya bisa dilakukan pada empty directory. Fungsi rekursif yang digunakan yaitu `rmtree()`

```c
void rmtree(const char path[])
{
    size_t path_len;
    char *full_path;
    DIR *dir;
    struct stat stat_path, stat_entry;
    struct dirent *entry;

    // stat for the path
    stat(path, &stat_path);

    // if path does not exists or is not dir - exit with status -1
    if (S_ISDIR(stat_path.st_mode) == 0) {
        fprintf(stderr, "%s: %s\n", "Is not directory", path);
        exit(-1);
    }

    // if not possible to read the directory for this user
    if ((dir = opendir(path)) == NULL) {
        fprintf(stderr, "%s: %s\n", "Can`t open directory", path);
        exit(-1);
    }

    // the length of the path
    path_len = strlen(path);

    // iteration through entries in the directory
    while ((entry = readdir(dir)) != NULL) {

        // skip entries "." and ".."
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, ".."))
            continue;

        // determinate a full path of an entry
        full_path = calloc(path_len + strlen(entry->d_name) + 1, sizeof(char));
        strcpy(full_path, path);
        strcat(full_path, "/");
        strcat(full_path, entry->d_name);

        // stat for the entry
        stat(full_path, &stat_entry);

        // recursively remove a nested directory
        if (S_ISDIR(stat_entry.st_mode) != 0) {
            rmtree(full_path);
            continue;
        }

        // remove a file object
        if (unlink(full_path) == 0)
            printf("Removed a file: %s\n", full_path);
        else
            printf("Can`t remove a file: %s\n", full_path);
        free(full_path);
    }

    // remove the devastated directory and close the object of it
    if (rmdir(path) == 0)
        printf("Removed a directory: %s\n", path);
    else
        printf("Can`t remove a directory: %s\n", path);

    closedir(dir);
}
```

Pada setiap fungsi akan dipanggil fungsi `update_log()` dan `update_log_rename()`(khusus 2 buah description). Fungsi tersebut berguna untuk melakukan pengisian pada file log sesuai proses yang dilakukan pada FUSE.

```c
int update_log(char *arg, const char *desc)
{
	time_t rawtime;
	struct tm *info;
	time( &rawtime );
	info = localtime( &rawtime );
	
	char infoF[10];
	if(strcmp(arg, "RMDIR")==0)
	{
		strcpy(infoF, "WARNING");
	}
	else
	{
		strcpy(infoF, "INFO");
	}
	
	FILE* file_ptr = fopen(dirLog, "a");	
	fprintf(file_ptr, "%s::%d%d%d-%02d:%02d:%02d::%s::%s\n", infoF, info->tm_mday, info->tm_mon+1, info->tm_year+1900, info->tm_hour, info->tm_min, info->tm_sec, arg, desc);
	
    fclose(file_ptr);
    return 0;
}

int update_log_rename(const char *desc1, const char *desc2)
{
	time_t rawtime;
	struct tm *info;
	time( &rawtime );
	info = localtime( &rawtime );
	
	FILE* file_ptr = fopen(dirLog, "a");	
	
	fprintf(file_ptr, "INFO::%d%d%d-%02d:%02d:%02d::RENAME::%s::%s\n", info->tm_mday, info->tm_mon+1, info->tm_year+1900, info->tm_hour, info->tm_min, info->tm_sec, desc1, desc2);
	
    fclose(file_ptr);
    return 0;
}
```


## Soal Shift 3
Ishaq adalah seseorang yang terkenal di kalangan anak informatika seluruh indonesia. Ia memiliki teman yang bernama innu dan anya, lalu ishaq bertemu dengan mereka dan melihat program yang mereka berdua kerjakan  sehingga ia tidak mau kalah dengan innu untuk membantu anya dengan menambah fitur yang ada pada programnya dengan ketentuan :

### A)
Jika suatu direktori dibuat dengan awalan “nam_do-saq_”, maka direktori tersebut akan menjadi sebuah direktori spesial

### B)
Jika suatu direktori di-rename dengan memberi awalan “nam_do-saq_”,maka direktori tersebut akan menjadi sebuah direktori spesial.

### C)
Apabila direktori yang terenkripsi di-rename dengan menghapus “nam_do-saq_” pada bagian awal nama folder maka direktori tersebut menjadi direktori normal.

### D)
Direktori spesial adalah direktori yang mengembalikan enkripsi/encoding pada direktori “Animeku_” maupun “IAN_” namun masing masing aturan mereka tetap berjalan pada direktori di dalamnya (sifat recursive “Animeku_” dan “IAN_” tetap berjalan pada subdirektori).

### E)
Pada direktori spesial semua nama file (tidak termasuk ekstensi) pada fuse akan berubah menjadi uppercase insensitive dan diberi ekstensi baru berupa nilai desimal dari biner perbedaan namanya.

**Contoh** : 
>jika pada direktori asli namanya adalah “isHaQ_KEreN.txt” maka pada fuse akan menjadi “ISHAQ_KEREN.txt.1670”. 1670 berasal dari biner 11010000110

### PENYELESAIAN

Langkah pertama adalah menginisialisasi string "nam_do-saq_" ke dalam array

```c
char nds[15] = "nam_do-saq_";
```

Kemudian dilakukan modifikasi mengenai pengecekkan nama file "nam_do-saq_" pada fungsi xmp_readdir, xmp_read yakni

```c
// Mencari string yang diinginkan
    char *letter3 = strstr(path, nds)

    // Melakukan pengecekan jika null
    if(letter3 != NULL)
    {
        letter3 += strlen(nds);
        char *temp = strchr(letter3, '/');
        if (temp != NULL)
            normFile(temp);
    }
```

Dan pada fungsi xmp_getattr yakni

```c
char *letter3 = strstr(path, nds);

    // Di cek jika letter3 tidak sama dengan null atau kosong
    if(letter3 != NULL)
    {
        letter3 += strlen(nds);
        char *temp = strchr(letter3, '/');
        if (temp != NULL)
        {
            normFile(letter3);
        }
    }
```

Langkah selanjutnya adalah memanggil fungsi normFile (pada fungsi xmp_getattr, xmp_read, xmp_readdir) untuk mengambil direktori normal. Fungsi ini juga dapat digunakan untuk mengembalikan direktori spesial menjadi direktori normal

```c
// Fungsi mengembalikan direktori spesial menjadi direktori biasa
void normFile(char *filename)
{
    // Checking jika kosong
    if((strcmp(filename, ".") == 0) || (strcmp(filename, "..") == 0)) 
    {
        return;
    }

    // Melakukan set blok memori
    char akhirFilename[1000];
    memset(akhirFilename, 0, sizeof(akhirFilename));

    // Membagi string
    char *token = strtok(filename, "/");
    while (token != NULL)
    {
        // Inisialisasi dan set blok memori
        char extension[1000], decimal[1000], newFile[1000];
        memset(extension, 0, sizeof(extension));
        memset(decimal, 0, sizeof(decimal));
        memset(newFile, 0, sizeof(newFile));
        
        int nExtension = 0, nDecimal = 0, nFile = 0, flag = 0;
        
        // Mengecek isi token dan menghitung flag
        for (int i = strlen(token) - 1; i >= 0; i--)
        {
            if (token[i] == '.' && flag == 0)
            {
                flag = 1;
                continue;
            }
            if (token[i] == '.' && flag == 1)
            {
                // Penempatan lokasi ekstensi
                extension(nExtension++) = token[i];
                flag = 2;
                continue;
            }
            if (flag == 1)
            {
                extension(nExtension++) = token[i];
            }
            else if (flag == 2)
            {
                newFile(nFile++) = token[i];
            }
            else   
            {
                decimal[nDecimal++] = token[i];
            }
        }

        // Inisialisasi blok memori ekstensi
        if (strcmp(newFile, "") == 0)
        {
            strcpy(newFile, extension)
            memset(extension, 0, sizeof(extension));
        }        

        // Inisialisasi desimal
        int vDecimal = atoi(decimal);
        int nBinary = 0;

        // Inisialisasi biner
        char binary[1000];
        memset(binary, 0, sizeof(binary));

        // Menghitung nilai biner dari desimal
        while (vDecimal > 0)
        {
            binary[nBinary++] = (char)(48 + vDecimal % 2);
            vDecimal /= 2;
        }

        // Mengembalikan ke nama file
        for (int i = 0; i < nBinary; i++)
        {
            if (binary[i] == '1')
            newFile[i] += 32;
        }

        // Menaruh ekstensi ke filename
        strcat(newFile, extension);
        strcat(akhirFilename, "/");
        strcat(akhirFilename, newFile);
        token = strtok(NULL, "/");
    }
    strcpy(filename, akhirFilename);
}
```

Untuk mengubah direktori normal menjadi direktori spesial, dipanggil fungsi specialFile (pada fungsi xmp_readdir)

```c
// Fungsi mengubah direktori biasa menjadi direktori spesial
void specialFile(char *filename) 
{    
    // Checking jika kosong
    if((strcmp(filename, ".") == 0) || (strcmp(filename, "..") == 0)) 
    {
        return;
    }

    // Inisialisasi dan set blok memori
    char extension[1000], binary[1000], newFile[1000];
    memset(extension, 0, sizeof(extension));       
    memset(binary, 0, sizeof(binary));
    memset(newFile, 0, sizeof(newFile));
        
    int nExtension = 0, nBinary = 0, nFile = 0, flag = 0;

    // Pengosongan newFile
    strcpy(newFile, "");
    
    // Menghitung flag
    for (int i = strlen(filename) - 1; i >= 0; i--)
    {
        if (filename[i] == '.' && flag == 0)
        {
            extension[nExtension++] = filename[i];
            flag = 1;
            continue;
        }
        if (flag == 1)
        {
            newFile[nFile++] = filename[i];
        }
        else
        {
            extension[nExtension++] = filename[i];
        }
    }
    
    // Cek jika newFile kosong
    if (strcmp(newFile, "") == 0)
    {
        // Inisialisasi dan set blok memori
        memset(newFile, 0, sizeof(newFile));
        strcpy(newFile, extension);
        memset(extension, 0, sizeof(extension));
        nFile = nExtension;
        nExtension = 0;
    }

    // Mengambil nilai binary
    for (int i = 0; i < nFile; i++)
    {
        if (newFile[i] >= 97 && newFile[i] <= 122)
        {
            binary[nBinary++] = '1';
            newFile[i] -= 32;
        }
        else    
        {
            binary[nBinary++] = '0';
        }
    }

    // Inisialisasi penambahan dan set blok memori nilai desimal
    int inc = 1, vDecimal = 0;
    char decimal[1000];
    memset(decimal, 0, sizeof(decimal));

    // Menghitung nilai desimal dari biner
    for (int i = nBinary - 1; i >= 0; i--)
    {
        if(binary[i] == '1')
            vDecimal += inc;
        inc *= 2;
    }
    
    // Menaruh ekstensi desimal
    sprintf(decimal, "%d", vDecimal);
    strcpy(filename, newFile);
    strcat(filename, extension);
    strcat(filename, ".");
    strcat(filename, decimal);
}
```

Agar rename dapat sesuai dengan ketentuan yang diminta, digunakan fungsi xmp_rename untuk sekaligus mencatat log direktori mana saja yang direname (nomor 1)

```c
// Fungsi rename menjadi direktori spesial
static int xmp_rename(const char *awal, const char *akhir)
{
    int res;
    char awalDir[1000], akhirDir[1000];
    char *p_awalDir, *p_akhirDir;

    // Mengambil direktori kondisi awal
    if (strcmp(awal, "/") == 0)
    {
        awal = dirpath;
        sprintf(awalDir, "%s", awal);
    }
    else 
    {
        sprintf(awalDir, "%s%s", dirpath, awal);
    }

    // Mengambil direktori kondisi akhir
    if (strcmp(source, "/") == 0)
    {
        sprintf(akhirDir, "%s", dirpath);
    }
    else
    {
        sprintf(akhirDir, "%s%s", dirpath, akhir);
    }

    // Melakukan rename
    res = rename(awalDir, akhirDir);

    //Mengembalikan pesan error
    if (res == -1)
    {
        return -errno;
    }

    p_awalDir = strrchr(awalDir, '/');
    p_akhirDir = strrchr(akhirDir, '/');

    if (strstr(p_akhirDir, "Animeku_"))
    {
        logged("RENAME", "terenkripsi", awalDir, akhirDir);
    }
    if (strstr(p_awalDir, "Animeku_"))
    {
        logged("RENAME", "terdecode", awalDir, akhirDir);
    }

return 0;
}

// Fungsi log 
// void createLog(const char *previousPath, const char *newPath)
// {
//     FILE *fileLog = fopen("home/kurniacf/Wibu.log", "a");
//     fprintf(fileLog, "%s --> %s\n", previousPath, newPath);
//     fclose(fileLog);
// }

// Fungsi log untuk no 1
void logged(char *perintah, char *jenis, char *lama, char *baru)
{
    FILE *fileLog;
    fileLog = fopen("home/kurniacf/Wibu.log", "a");
    
    // Jika gagal dalam membuka
    if (fileLog == NULL)
    {
        printf("Error");
        exit(1);
    }

    // Jika berhasil dalam membuka file, print ke dalam fileLog (Wibu.log)
    fprintf(fileLog, "%s\t%s\t%s\t-->\t%s\n", perintah, jenis, lama, baru);
    fclose(fileLog);
}
```

Terakhir, inisialisasi struct dimodifikasi dengan menambahkan rename

```c
// inisialisai struct
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename
};
```


