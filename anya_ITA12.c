#define FUSE_USE_VERSION 28
#ifndef PROF_FILE_OFFSET_BITS
#define PROF_FILE_OFFSET_BITS 64
#endif
#include <proc_service.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <pthread.h>
#include <ctype.h>
#include <fuse.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <stdbool.h>

static const char *dirpath = "/home/lax/Documents";
static const char *dirLog = "/home/lax/hayolongapain_ITA12.log";

// Inisialisasi nama directory
char typeName[10] = "Animeku_";
char ian[10] = "IAN_";
char nds[15] = "nam_do-saq_";
char innu[15] = "INNUGANTENG";

void rmtree(const char path[])
{
    size_t path_len;
    char *full_path;
    DIR *dir;
    struct stat stat_path, stat_entry;
    struct dirent *entry;

    // stat for the path
    stat(path, &stat_path);

    // if path does not exists or is not dir - exit with status -1
    if (S_ISDIR(stat_path.st_mode) == 0) {
        fprintf(stderr, "%s: %s\n", "Is not directory", path);
        exit(-1);
    }

    // if not possible to read the directory for this user
    if ((dir = opendir(path)) == NULL) {
        fprintf(stderr, "%s: %s\n", "Can`t open directory", path);
        exit(-1);
    }

    // the length of the path
    path_len = strlen(path);

    // iteration through entries in the directory
    while ((entry = readdir(dir)) != NULL) {

        // skip entries "." and ".."
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, ".."))
            continue;

        // determinate a full path of an entry
        full_path = calloc(path_len + strlen(entry->d_name) + 1, sizeof(char));
        strcpy(full_path, path);
        strcat(full_path, "/");
        strcat(full_path, entry->d_name);

        // stat for the entry
        stat(full_path, &stat_entry);

        // recursively remove a nested directory
        if (S_ISDIR(stat_entry.st_mode) != 0) {
            rmtree(full_path);
            continue;
        }

        // remove a file object
        if (unlink(full_path) == 0)
            printf("Removed a file: %s\n", full_path);
        else
            printf("Can`t remove a file: %s\n", full_path);
        free(full_path);
    }

    // remove the devastated directory and close the object of it
    if (rmdir(path) == 0)
        printf("Removed a directory: %s\n", path);
    else
        printf("Can`t remove a directory: %s\n", path);

    closedir(dir);
}

int make_log_file()
{
	FILE* file_ptr = fopen(dirLog, "a");
    fclose(file_ptr);
    return 0;
}

int update_log(char *arg, const char *desc)
{
	time_t rawtime;
	struct tm *info;
	time( &rawtime );
	info = localtime( &rawtime );
	
	char infoF[10];
	if(strcmp(arg, "RMDIR")==0)
	{
		strcpy(infoF, "WARNING");
	}
	else
	{
		strcpy(infoF, "INFO");
	}
	
	FILE* file_ptr = fopen(dirLog, "a");	
	fprintf(file_ptr, "%s::%d%d%d-%02d:%02d:%02d::%s::%s\n", infoF, info->tm_mday, info->tm_mon+1, info->tm_year+1900, info->tm_hour, info->tm_min, info->tm_sec, arg, desc);
	
    fclose(file_ptr);
    return 0;
}

int update_log_rename(const char *desc1, const char *desc2)
{
	time_t rawtime;
	struct tm *info;
	time( &rawtime );
	info = localtime( &rawtime );
	
	FILE* file_ptr = fopen(dirLog, "a");	
	
	fprintf(file_ptr, "INFO::%d%d%d-%02d:%02d:%02d::RENAME::%s::%s\n", info->tm_mday, info->tm_mon+1, info->tm_year+1900, info->tm_hour, info->tm_min, info->tm_sec, desc1, desc2);
	
    fclose(file_ptr);
    return 0;
}

void Cipher(char *word)
{
	
	char key[11]="INNUGANTENG";
	int input_len = strlen(word);
	for(int i=0; i<input_len; i++)
	{
		int j = i%11;
		if(word[i]>='A' && word[i]<='Z')
		{
			word[i]=(((word[i]-65)+(key[j]-65))%26)+65;
		}
		if(word[i]>='a' && word[i]<='z')
		{
			word[i]=(((word[i]-97)+(key[j]-65))%26)+97;
		}
	}
//Changes the word
}

void deCipher(char *word)
{
	
	char key[11]="INNUGANTENG";
	int input_len = strlen(word);
	for(int i=0; i<input_len; i++)
	{
		int j = i%11;
		if(word[i]>='A' && word[i]<='Z')
		{
			word[i]=(((word[i]-65)-(key[j]-65)+26)%26)+65;
		}
		else if(word[i]>='a' && word[i]<='z')
		{
			word[i]=(((word[i]-97)-(key[j]-65)+26)%26)+97;
		}
	}
//Changes the word
}

void readSubdir(const char *path, int resStat)
{
	DIR *dp;
    struct dirent *de;

    dp = opendir(path);

    if (dp == NULL) return;

    while ((de = readdir(dp)) != NULL) {
    	if(strcmp(de->d_name,".") && strcmp(de->d_name,".."))
    	{
    		if (de->d_type == DT_DIR)
            {
                char newpath[1024];

                strcpy(newpath, path);
                strcat(newpath, "/");
                strcat(newpath, de->d_name);

                readSubdir(newpath,resStat);

    			memset(newpath,0,strlen(newpath));
            }
    		char pathName1[1024];
    		char pathName2[1024];
    		char newName[50];
    		strcpy(newName, de->d_name);
    		(resStat == 0) ? Cipher(newName) : deCipher(newName);
    		
    		strcpy(pathName2, path);
    		strcat(pathName2,"/");
    		strcat(pathName2,newName);
    		
    		strcpy(pathName1, path);
    		strcat(pathName1,"/");
    		strcat(pathName1,de->d_name);
    		rename(pathName1, pathName2);
    		
    		memset(pathName1,0,strlen(pathName1));
    		memset(pathName2,0,strlen(pathName2));
    		memset(newName,0,strlen(newName));
		}
    }
    closedir(dp);
    return;
}

// Fungsi untuk encode 
void encode(char *filename) 
{
    // Inisialisasi
    int index, i = 0, countAlphabet = 26;

    // Checking jika kosong
    if((strcmp(filename, ".") == 0) || (strcmp(filename, "..") == 0)) 
    {
        return;
    }

    // Hitung jumlah char
    index = strlen(filename);

    // Membedah setiap char
    while (filename[i] != index + 1)
    {
        // Jika ada whitespace, maka continue dan index akan berlanjut
        if(filename[i] == ' ')
        {
            i++;
            continue;
        }

        // Jika Huruf Besar dengan ASCII (65 <= x <= 90)
        if(filename[i] >= 'A' && filename[i] <= 'Z')
        {
            // Di encode menggunakan ALbash Cipher
            for(int j = 0; j < index; j++)
            {
                int start = filename[j];
                filename[j] = countAlphabet - filename[j] - 64;
                filename[j] += 65;
            }
        } else if (filename[i] >= 'a' && filename[i] <= 'z') // Jika huruf kecil dengan ASCII (97 <= x <= 122)
        {
            // Di encode menggunakan rot13
            // Carriage Return = 13 -> ASCII ('CR' = 13)
            if(filename[i] + 13 > 'z')
            {
                filename[i] -= 13;
            } else 
            {
                filename[i] += 13;
            }
        }
        // lanjut ke index selanjutnya
        i++;
    }
}

// Fungsi untuk decode
void decode(char *filename) 
{
    // Inisialisasi
    int index, i = 0, countAlphabet = 26;

    // Checking jika kosong
    if((strcmp(filename, ".") == 0 ) || (strcmp(filename, "..") == 0) || (strstr(filename, "/") == NULL)) 
    {
        return;
    }

    // Hitung jumlah char keseluruhan
    index = strlen(filename);

    // Untuk mencari teks jika ada "/"
    char *name = strstr(filename, "/");

    // Membedah setiap char
    while (filename[i] != index + 1)
    {
        // Jika ada tanda /, maka akan berhenti
        if(filename[i] == '/')
        {
            break;
        }
        
        // Jika ada tanda ., maka akan berhenti dan index disimpan dengan i yang sekarang
        if(filename[i] == '.')
        {
            index = i;
            break;
        }

        // Jika Huruf Besar dengan ASCII (65 <= x <= 90)
        if(filename[i] >= 'A' && filename[i] <= 'Z')
        {
            // Meng-decode menggunakan ALbash Cipher
            for(int j = 0; j < index; j++)
            {
                int start = name[j] + 1;
                name[j] = countAlphabet - name[j] + 1 - 64;
                name[j] += 65;
            }
        } else if (filename[i] >= 'a' && filename[i] <= 'z') // Jika huruf kecil dengan ASCII (97 <= x <= 122)
        {
            // Meng-decode menggunakan rot13
            // Carriage Return = 13
            if(filename[i] - 13 > 'a')
            {
                filename[i] += 13;
            } else 
            {
                filename[i] -= 13;
            }
        }
        i++;
    }
}

// Fungsi mengubah direktori biasa menjadi direktori spesial
void specialFile(char *filename) 
{    
    // Checking jika kosong
    if((strcmp(filename, ".") == 0) || (strcmp(filename, "..") == 0)) 
    {
        return;
    }

    // Inisialisasi dan set blok memori
    char extension[1000], binary[1000], newFile[1000];
    memset(extension, 0, sizeof(extension));       
    memset(binary, 0, sizeof(binary));
    memset(newFile, 0, sizeof(newFile));
        
    int nExtension = 0, nBinary = 0, nFile = 0, flag = 0;

    // Pengosongan newFile
    strcpy(newFile, "");
    
    // Menghitung flag
    for (int i = strlen(filename) - 1; i >= 0; i--)
    {
        if (filename[i] == '.' && flag == 0)
        {
            extension[nExtension++] = filename[i];
            flag = 1;
            continue;
        }
        if (flag == 1)
        {
            newFile[nFile++] = filename[i];
        }
        else
        {
            extension[nExtension++] = filename[i];
        }
    }

    // Cek jika newFile kosong
    if (strcmp(newFile, "") == 0)
    {
        // Inisialisasi dan set blok memori
        memset(newFile, 0, sizeof(newFile));
        strcpy(newFile, extension);
        memset(extension, 0, sizeof(extension));
        nFile = nExtension;
        nExtension = 0;
    }

    // Mengambil nilai binary
    for (int i = 0; i < nFile; i++)
    {
        if (newFile[i] >= 97 && newFile[i] <= 122)
        {
            binary[nBinary++] = '1';
            newFile[i] -= 32;
        }
        else    
        {
            binary[nBinary++] = '0';
        }
    }

    // Inisialisasi penambahan dan set blok memori nilai desimal
    int inc = 1, vDecimal = 0;
    char decimal[1000];
    memset(decimal, 0, sizeof(decimal));

    // Menghitung nilai desimal dari biner
    for (int i = nBinary - 1; i >= 0; i--)
    {
        if(binary[i] == '1')
            vDecimal += inc;
        inc *= 2;
    }
    
    // Menaruh ekstensi desimal
    sprintf(decimal, "%d", vDecimal);
    strcpy(filename, newFile);
    strcat(filename, extension);
    strcat(filename, ".");
    strcat(filename, decimal);
}

// Fungsi mengembalikan direktori spesial menjadi direktori biasa
void normFile(char *filename)
{
    // Checking jika kosong
    if((strcmp(filename, ".") == 0) || (strcmp(filename, "..") == 0)) 
    {
        return;
    }

    // Melakukan set blok memori
    char akhirFilename[1000];
    memset(akhirFilename, 0, sizeof(akhirFilename));

    // Membagi string
    char *token = strtok(filename, "/");
    while (token != NULL)
    {
        // Inisialisasi dan set blok memori
        char extension[1000], decimal[1000], newFile[1000];
        memset(extension, 0, sizeof(extension));
        memset(decimal, 0, sizeof(decimal));
        memset(newFile, 0, sizeof(newFile));
        
        int nExtension = 0, nDecimal = 0, nFile = 0, flag = 0;
        
        // Mengecek isi token dan menghitung flag
        for (int i = strlen(token) - 1; i >= 0; i--)
        {
            if (token[i] == '.' && flag == 0)
            {
                flag = 1;
                continue;
            }
            if (token[i] == '.' && flag == 1)
            {
                // Penempatan lokasi ekstensi
                extension(nExtension++) = token[i];
                flag = 2;
                continue;
            }
            if (flag == 1)
            {
                extension(nExtension++) = token[i];
            }
            else if (flag == 2)
            {
                newFile(nFile++) = token[i];
            }
            else   
            {
                decimal[nDecimal++] = token[i];
            }
        }

        // Inisialisasi blok memori ekstensi
        if (strcmp(newFile, "") == 0)
        {
            strcpy(newFile, extension)
            memset(extension, 0, sizeof(extension));
        }        

        // Inisialisasi desimal
        int vDecimal = atoi(decimal);
        int nBinary = 0;

        // Inisialisasi biner
        char binary[1000];
        memset(binary, 0, sizeof(binary));

        // Menghitung nilai biner dari desimal
        while (vDecimal > 0)
        {
            binary[nBinary++] = (char)(48 + vDecimal % 2);
            vDecimal /= 2;
        }

        // Mengembalikan nama direktori
        for (int i = 0; i < nBinary; i++)
        {
            if (binary[i] == '1')
            newFile[i] += 32;
        }

        // Menaruh ekstensi ke filename
        strcat(newFile, extension);
        strcat(akhirFilename, "/");
        strcat(akhirFilename, newFile);
        token = strtok(NULL, "/");
    }
    strcpy(filename, akhirFilename);
}

// Fungsi mengambil atribute file
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char newPath[1000];

    // Mengambil atribute no 1
    char *letter = strstr(path, typeName);

    // Di cek jika letter tidak sama dengan null atau kosong
    if(letter != NULL)
    {
        decode(letter);
        // encode(letter);
    }

    // Mengambil atribute no 3
    char *letter3 = strstr(path, nds);

    // Di cek jika letter3 tidak sama dengan null atau kosong
    if(letter3 != NULL)
    {
        letter3 += strlen(nds);
        char *temp = strchr(letter3, '/');
        if (temp != NULL)
        {
            normFile(letter3);
        }
    }

    if (strcmp(path, "/") == 0) 
    {
        path = dirpath;
        sprintf(newPath, "%s", path);
    }
    else 
    {
        sprintf(newPath, "%s%s", dirpath, path);
    }

    // memberikan informasi rinci tentang file
    res = lstat(newPath, stbuf);

    // mengembalikan kode kesalahan terakhir
    if(res == -1)
    {
        return -errno;
    }
    
    return 0;
}

// Fungsi membaca directory
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    // Inisialisasi
    int res = 0;
    char fpatch[1000];
    struct dirent *dir;

    // Mencari string yang diinginkan
    char *letter = strstr(path, typeName);
    
    // Melakukan pengecekan jika null
    if(letter != NULL)
    {
        decode(letter);
    }

    // Mencari string yang diinginkan
    char *letter3 = strstr(path, nds)

    // Melakukan pengecekan jika null
    if(letter3 != NULL)
    {
        letter3 += strlen(nds);
        char *temp = strchr(letter3, '/');
        if (temp != NULL)
            normFile(temp);
    }

    // Jika string terdapat '/' maka nilai path akan menjadi dirpath
    if(strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    // Inisialisasi
    DIR *dp;
    (void) fi;
    (void) offset;

    // Membuka directory
    dp = opendir(fpath);

    // mengembalikan kode kesalahan terakhir
    if(dp == NULL) 
    {
        return -errno;
    }

    // Membaca directory
    dir = readdir(dp);

    // Jika tidak null
    while (dir != NULL)
    {
        if(strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0)
            continue;

        // Inisialisasi
        struct stat st;

        // menetapkan nilai satu byte ke blok memori byte demi byte.
        memset(&st, 0, sizeof(st));
        st.st_ino = dir->d_ino;
        st.st_mode = dir->d_type << 12;

        // Cek jika tidak null
        if(letter != NULL)
        {
            encode(dir->d_name);
        }

        if(letter3 != NULL)
        {
            specialFile(dir->d_name);
        }

        res = filler(buf, dir->d_name, &st, 0);
        if(res != 0 )
        {
            break;
        }
    }

    // Tutup directory
    closedir(dp);
    return 0;
}

// Membaca data dari file yang dibuka
static int xmp_read (const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    / Mencari string yang diinginkan
    char *letter = strstr(path, typeName);
    
    // Melakukan pengecekan jika null
    if(letter != NULL)
    {
        decode(letter);
    }

    // Mencari string yang diinginkan
    char *letter3 = strstr(path, nds)

    // Melakukan pengecekan jika null
    if(letter3 != NULL)
    {
        letter3 += strlen(nds);
        char *temp = strchr(letter3, '/');
        if (temp != NULL)
            normFile(temp);
    }

    // Inisialisasi
    char fpath[1000];
    int fd, res;

    if (strcmp(path, "/") == 0) 
    {
        path = dirpath;
        sprintf(newPath, "%s", path);
    }
    else 
    {
        sprintf(newPath, "%s%s", dirpath, path);
    }
    (void)fi;

    // Membuka path
    fd = open(fpath, O_RDONLY);

    // Mengirimkan error sesuai kode error terakhir
    if(fd == -1)
    {
        return -errno;
    }

    // Untuk membaca file
    res = pread(fd, buf, size, offset);

    // Mengirimkan error sesuai kode error terakhir
    if(res == -1)
    {
        res = -errno;
    }

    // menutup file
    close(fd);
    return res;
}

// Fungsi rename menjadi direktori spesial
static int xmp_rename(const char *awal, const char *akhir)
{
    int res;
    char awalDir[1000], akhirDir[1000];
    char *p_awalDir, *p_akhirDir;
    int foldStat = strncmp(akhir+1, "IAN_", 4);

    // Mengambil direktori kondisi awal
    if (strcmp(awal, "/") == 0)
    {
        awal = dirpath;
        sprintf(awalDir, "%s", awal);
    }
    else 
    {
        sprintf(awalDir, "%s%s", dirpath, awal);
    }

    // Mengambil direktori kondisi akhir
    if (strcmp(source, "/") == 0)
    {
        sprintf(akhirDir, "%s", dirpath);
    }
    else
    {
        sprintf(akhirDir, "%s%s", dirpath, akhir);
    }

    // Melakukan rename
    res = rename(awalDir, akhirDir);

    //Mengembalikan pesan error
    if (res == -1)
    {
        return -errno;
    }

    p_awalDir = strrchr(awalDir, '/');
    p_akhirDir = strrchr(akhirDir, '/');

    if (strstr(p_akhirDir, "Animeku_"))
    {
        logged("RENAME", "terenkripsi", awalDir, akhirDir);
    }
    if (strstr(p_awalDir, "Animeku_"))
    {
        logged("RENAME", "terdecode", awalDir, akhirDir);
    }

    DIR *dp;
    struct dirent *de;

    dp = opendir(akhirDir);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
    	if(strcmp(de->d_name,".") && strcmp(de->d_name,".."))
    	{
    		if (de->d_type == DT_DIR)
            {
                char newpath[1024];

                strcpy(newpath, akhirDir);
                strcat(newpath, "/");
                strcat(newpath, de->d_name);

                readSubdir(newpath, foldStat);

    			memset(newpath,0,strlen(newpath));
            }
    		char pathName1[1024];
    		char pathName2[1024];
    		char newName[50];
    		strcpy(newName, de->d_name);
    		(foldStat == 0) ? Cipher(newName) : deCipher(newName);
    		
    		strcpy(pathName2, akhirDir);
    		strcat(pathName2,"/");
    		strcat(pathName2,newName);
    		
    		strcpy(pathName1, akhirDir);
    		strcat(pathName1,"/");
    		strcat(pathName1,de->d_name);
    		rename(pathName1, pathName2);
    		
    		memset(pathName1,0,strlen(pathName1));
    		memset(pathName2,0,strlen(pathName2));
    		memset(newName,0,strlen(newName));
		}
    }
    closedir(dp);

return 0;
}

// Fungsi log 
// void createLog(const char *previousPath, const char *newPath)
// {
//     FILE *fileLog = fopen("home/kurniacf/Wibu.log", "a");
//     fprintf(fileLog, "%s --> %s\n", previousPath, newPath);
//     fclose(fileLog);
// }

// Fungsi log untuk no 1
void logged(char *perintah, char *jenis, char *lama, char *baru)
{
    FILE *fileLog;
    fileLog = fopen("home/kurniacf/Wibu.log", "a");
    
    // Jika gagal dalam membuka
    if (fileLog == NULL)
    {
        printf("Error");
        exit(1);
    }

    // Jika berhasil dalam membuka file, print ke dalam fileLog (Wibu.log)
    fprintf(fileLog, "%s\t%s\t%s\t-->\t%s\n", perintah, jenis, lama, baru);
    fclose(fileLog);
}

static int xmp_mkdir (const char *name, mode_t dirMode)
{
	update_log("MKDIR", name);
	char *path= (char*) malloc(strlen(dirpath) + strlen(name) + 2);
	sprintf(path, "%s%s",dirpath,name);
	mkdir(path, dirMode);
	return 0;
}

static int xmp_rmdir (const char *name)
{
	char *nameFold = (char*) malloc(strlen(name));
	strcpy(nameFold, name);
	update_log("RMDIR", nameFold);
	char *path= (char*) malloc(strlen(dirpath) + strlen(name) + 2);
	sprintf(path, "%s%s",dirpath,name);
	rmtree(path);
	return 0;
}

// inisialisai struct
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
    .mkdir = xmp_mkdir,
    .rmdir = xmp_rmdir
};

// Fungsi main
int main(int argc, char *argv[]) {
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}
